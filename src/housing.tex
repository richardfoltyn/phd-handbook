\chapter{Finding accommodation}

This is the most painful part of moving to Sweden, as renting an apartment
or a room in Stockholm is more difficult and more expensive than in many
other European cities. Not surprisingly, many Swedes prefer to buy apartments,
even if they plan to live in them for only a few years.

Make sure you are settled before the program starts at
the end of August, so housing trouble does not distract you from your studies.
Whatever arrangements you make (except for buying your own apartment), you
will most likely move several times during your studies. Hence there is nothing
wrong with initially getting only temporary accommodation for a few months.

\section{SSSB}
\label{sec:living:sssb}

\marginnote{SSSB website: \url{http://www.sssb.se}}
SSSB (Stiftelsen Stockholms Studentbostäder) is jointly owned and run by
student unions in Stockholm.
They offer rooms and apartments in all areas of Stockholm including the suburbs,
at prices ranging from \sek{2,500--6,000}. You need to be a member of a student union
(see \ref{sec:living:sus}) and have sufficient days in the queue (``credit'') 
to be able to get a 
first-hand contract. However, when you are admitted to the PhD program you
will have zero credit and thus you will not be eligible to get
housing from from SSSB.
\marginnote[-2em]{The minimum number of days needed for the least desirable 
accommodation is about 300.}

\paragraph{Types of accommodation.}
To understand how SSSB might still be of use even initially, consider the
types of housing available:
\begin{compactenum}

  \item \emph{Corridor rooms} (``studentrum'')
    have an own bathroom/toilet but the corridor kitchen is shared with
    10--15 other students. You are not permitted to share a corridor room.
    
  \item \emph{Studios} (``studentetta'') are single-room apartments   
  	with a small kitchenette, bathroom and toilet. These may be 
  	inhabited by one or two people, even though the contract is
  	awarded to a single person.
  	
  \item \emph{Apartments} with two or more rooms (``studentlägenhet'') are
  	only awarded to two tenants, possibly with dependent children.

\end{compactenum}

Thus in order to live with SSSB without having sufficient credit yourself, you can
either
\begin{compactitem}

  \item find someone who has a first-hand contract 
    and is willing to rent out his/her room or studio 
    (possibly because this person is going abroad); or
    
  \item find someone who is the main tenant of a two-room apartment and is
    looking for a second person (``co-tenant'') to share the apartment with.
    
    This arrangement has the added benefit that you can yourself become
    the main tenant after a year once the original tenant moves out.
    For this to work, the main tenant must have gotten the apartment via regular 
    credit days (\ie not by moving in as a co-tenant), and
    you must be registered with SSSB as a co-tenant when you move in.

\end{compactitem}

\paragraph{Registering with SSSB.}
\marginnote[1em]{You can get up to 90 ``free'' credit days without being 
a student union member.}
Regardless of whether you are able to find someone who is willing to
rent out (part of) their SSSB accommodation, you should in any
case register with SSSB, even if you do not plan 
to live with them in the initial years of your PhD. After accumulating
sufficiently many days you will eventually be able to get decent apartments
below market price.

Formally, you need to be a member of a student union to be registered with
SSSB. However, since you cannot become a member before coming to Sweden
and enrolling at the university, SSSB has a 90-day grace period. Therefore,
\begin{compactenum}

  \item Sign up with SSSB immediately, without having a Swedish \pn
    (see \ref{sec:living:pn}) or being a student union member;

  \item Collect up to 90 credit days before coming to Stockholm. 
    If you have not become a student union member within 90 days,
    you should put your queue on hold after 90 days until you do so.

  \item Once you enrolled at the university, register with the
    student union (see \ref{sec:living:sus}). Make sure that you
    use the same email address for the student union and SSSB,
    as union membership is matched by email address if you
    have no \pn.

  \item After receiving your \pn (see \ref{sec:living:pn}), make sure to update it in 
    your SSSB profile.

  \item Note that you need to log into your SSSB account regularly
    (once every 90 days) to keep your queue status active!

\end{compactenum}

\section{Accommodation via \su}
\marginnote{Website: \url{http://www.su.se/english/staff-info/new-employee/living-in-sweden/accommodation-for-visiting-researchers/housing-areas}}

In the past years \su has started to provide a limited number of
rooms to PhD students. Note that these apartments are available only for a 
limited time (currently this seems to be two years).

\section{Renting from other PhDs in the program}

Many PhD students eventually spend a year abroad and are interested in
renting out their room/apartment. If you learn of such an opportunity,
you should definitely consider it as it solves your housing problems
for one or two terms.

\section{Bostadsförmedlingen i Stockholm}
\label{sec:housing:bostadsf}

\marginnote{Website: \url{https://bostad.stockholm.se}}

Stockholm City has its own queue for apartments for rent
(these can be publicly or privately owned). You need several years in the queue
to be able to book any apartments, so this will be of no immediate use to you.
You should sign up if you plan to stay in Sweden after your PhD, however,
in case you do not want to buy an apartment.
There is an annual fee of approx. \sek{200}.

They also have separate offers for student housing, which can be located
in buildings owned by other companies (such as Svebo, see \ref{sec:housing:svebo}), 
but managed by Bostadsförmedlingen.


\section{Svebo}
\label{sec:housing:svebo}
\marginnote{Website: \url{https://www.svenskabostader.se}}

Svenska bostäder (Svebo) is a real estate company that provides approximately 
750 apartments for students. It seems that these days the queues for their
student housing are managed by Bostadsförmedlingen (see \ref{sec:housing:bostadsf}),
but there is some information available on their site as well.

\section{blocket}
\marginnote{Website: \url{http://www.blocket.se} (only in Swedish)}

Blocket is the largest Swedish website for classified ads of all sorts,
including renting out accommodation. 
These days they have a separate section of
their website dedicated to housing which you find by clicking on ``bostad''.

The website is of limited use to foreigners for two reasons:
\begin{compactenum}
  \item the site and most ads are in Swedish; (you can use automatic Google
    translate supported by some browsers such as Google Chrome)
  \item you most likely need a Nordic credit card to place an ad. However, you
    could ask someone living in Sweden (such as future colleagues or your buddy)
    to place an ad for you.
\end{compactenum}

Despite these drawbacks, given that this site is so popular among Swedes it
might still be worth it. Several foreign students in the program
got at least temporary initial accommodation by posting ads (in English or Swedish --
ask your buddy to translate if necessary).

When trying to contact potential landlords, also keep in mind that due to
the housing market in Stockholm apartments can be rented out within days.

\section{Bostad Direkt}
\marginnote{Website: \url{http://bostaddirekt.com}}

Bostad Direkt is one of the largest agencies for subletting, i.e. for renting an apartment or room from a private person (rather than public housing). 

Its major drawback is that you have to pay for using it. In fact, it is possible to search for potential accommodations without being registered. Once you’ve found an interesting offer, you will have to register in order to get the contact information. In order to get information about flats in the Stockholm area, you have to pay \sek{700}, and it is valid only for 45 days.

Once you're registered, you can start to contact landlords. If they post both their number and email address, calling them is more effective, as very often emails are not answered. 
Don't worry if you don't speak Swedish: in the majority of the cases they understand English. 

You should check the page frequently for new offers and try to contact landlords as soon as possible: often landlords find tenants within days.

A second way to use Bostad Direkt is posting your own ad, with all your information and what kind of place you're looking for. 
It can definitely be a good strategy, as some landlords might not bother to write their own ad, but instead just scan through the ads of potential tenants.

\section{Akademisk kvart}
\marginnote{Website: \url{http://akademiskkvart.se}}

Akademisk kvart is a website where owners and other students post their ads for renting apartments/rooms to students. 
The webpage is also available in English, and you can apply online or contact the landlord via phone/email. 
Given the target group, it lists more offers suited for students, 
and it does not seem too hard to get in contact with landlords through this website.

\section{lappis.org}
\marginnote{Website: \url{http://www.lappis.org}}

\url{lappis.org} (short for ``Lappkärrsberget'', the SSSB housing area closest to
\su) is an online forum where students post all sorts of ads, including
those who want to sublet their (mostly SSSB) apartments. Despite the name,
these apartments do not have to be located in Lappis.

Note that you can only get second-hand contracts with limited (or no) rights,
since SSSB usually does not allow subletting.
\marginnote{SSSB permits subletting only under special circumstances, \eg
when students go abroad.}
On the other hand, SSSB does not actively check whether someone sublet their
apartment, and many students do it.
Beware that since no one checks the authenticity of offers placed on this site,
some of them might be dubious. In no case should you transfer money before inspecting 
the place you want to rent.

\section{Living in the suburbs}

If you cannot find reasonably priced housing in Stockholm, consider
living the northern suburbs such as Danderyd or Täby. Traveling time by
bus or Roslagsbanan (a train network) amounts to about 30--45 min to the university,
and renting a room or part of a house there is cheaper than in central Stockholm.

