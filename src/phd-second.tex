\chapter{Later years}

\section{Courses}

There is a wide selection of second-year courses (``field courses'') covering 
diverse research
interests (with the exception of theoretical econometrics). The grading scheme
varies widely, ranging from courses with exams and problem sets to courses
where you write term papers or referee reports. Note that at least five students are
required for a course to take place. On top of the courses offered at SU and SSE,
you may also take courses at Uppsala University.

Since you need 105 ECTS credits in total, you will usually need to select six
of these field courses (of 7.5 ECTS credits each) on top of your first-year courses.

\section{Teaching}

Starting from the second year, you have the opportunity to teach as a TA 
(you are not permitted to be the main lecturer in a course). Teaching at \su is not mandatory,
but may be required to get extended funding beyond the first four years if you are at
the \dept.

There are two types of teaching opportunities:
\begin{compactenum}
	\item Undergraduate courses offered by the \dept will usually extend the
		number of years you are entitled to receive funding (see \ref{sec:phd:funding}). 
		A list of available teaching positions is sent out a few months in advance.
	\item You can also TA first-year PhD-level courses. 
		\marginnote{Except for Political Economics, second-year courses have no TA sessions.}
		TA-ing these courses is usually by invitation, 
		so students who performed well may be asked should any such position
		be available. Until recently, no funding extensions were awarded for TA-ing
		PhD courses, but now students at the department are eligible for prolonged funding.
		Non-department students, on the other hand, will receive a salary that amounts to
		approx. \sek{5000} after taxes for a course with four TA sessions.
		The effective hourly wage is quite low, so the main motivation to accept such TA positions
		is to familiarize yourself with the subject more thoroughly and put the
		teaching experience on your CV.
\end{compactenum}


\section{Transferring to \iies or \sofi}
\label{sec:phd:transfer}

\subsection{\iies}
\label{sec:phd:transfer:iies}

\marginnote{Graduate student position are announced online at 
	\url{http://www.iies.su.se/about-us/vacancies}. In addition, the announcement is usually set out
	by email to students at SU.}
	
If you are interested in Political Economics, Development Economics or Macroeconomics, you should consider applying to the \iies. For other topics (e.g. trade or labor economics), 
the \dept may be the better match. 

To be admitted as graduate student at the \iies, you have to be selected through an application process.
Admission usually takes place at the beginning of June. 
Students who are RAs at the \iies during their first two years must also apply in order to remain at the \iies after their second year.

Students typically start at the \iies in their third (or fourth) year, as you are supposed to have taken most of your courses before you apply. 
In a few cases students at the end of their first year have been admitted as well.

\subsection{\sofi}
\label{sec:phd:transfer:sofi}

If you become interested in teaming up and intend to focus your research on a topic that overlaps with research done at \sofi, you can contact one of the professors, or some other member of the research staff at \sofi. If you find a supervisor from \sofi, you may consider to become a PhD student at \sofi. There is no formal schedule on how and when to apply for a position as PhD student at \sofi.

PhD students at \sofi are usually involved in ongoing projects of their supervisors. 
They might be asked if they are interested in teaching (\eg undergraduate labor economics), but teaching is not mandatory.

\section{Seminars and study groups}

From your second year onward you should attend the seminar series offered
by your institution. Attendance is not mandatory at the \dept, but highly
encouraged. At the \iies, graduate students are expected to attend all seminars.
You are also free to attend the seminar series of the other two institutions
at \su, or even at SSE. Seminars are announced on each institution's website.

There are two types of seminars:
\begin{compactenum}
	\item Regular seminars are usually given by external invited speakers from all over the world;
	\item Brown-bag seminars are used to present work in progress by faculty members and graduate
		students. You can start signing up for brown-bag seminars in your second year.
\end{compactenum}

On top of the seminar series, there are various other reading or research groups with
regular presentations:
\begin{compactitem}
	\item The \emph{Macro reading group} is organized by students who present influential 
		papers in macroeconomics. Students meet every two weeks on average.
	\item The \emph{``Microwave''} is a group for students to present their research progress in
		all areas of applied microeconometrics, including development, labor and behavioral economics
		as well as Political Economy. The group meets every 2--3 weeks.
	\item The \emph{Macro group} at \iies is organized as a research group where students
		mostly supervised by \iies faculty present their research progress.
\end{compactitem}

\section{Supervisors}
\marginnote{Formally, the current director of graduate studies is your supervisor
	until you find one yourself.}
One of the central tasks in your second year is to choose a supervisor. Unlike with 
many other programs in Sweden, you are not assigned a supervisor or mentor during your
first year.

There is no formal requirement to have a supervisor from your institution. 
However, it is advisable to have either the supervisor or co-supervisor from your institution.
This is due to funding, but also to time constraints: it can happen that professors prioritize students from their institution if their time is limited. 

At the beginning of each academic year, some form of supervisor presentations are organized
where potential supervisors from all three institutions present their research interests
and ongoing projects. 
These events are primarily targeting students starting their second year and should
``kick-start'' the process of finding a supervisor.

\section{Funding}
\label{sec:phd:funding}

\emph{Disclaimer: The information presented below has been
compiled by the Graduate Students' Council and is meant to
given an overview of the current funding situation. 
Needless to say, neither the
Department of Economics nor the IIES is bound by anything written below,
and thus your actual funding might differ.}

\subsection{\dept}

The stipend / salary system at the department is somewhat complicated and 
undergoing repeated change due to new regulations at the university and 
national level. Currently there are three major funding schemes:

\begin{enumerate}

  \item \label{item:funding:donation}
    \marginnote{See Kammerkollegiet's website at
    \url{http://www.kammarkollegiet.se/english/insurance/doctoral-students-stipends}
    for information on how to claim parental and sick leave benefits.}
    
    Initially, students are financed by so-called \emph{donation stipends}
    which amount to \sek{12,500} a month of non-taxable income.
    Since these are not a form of employment, social insurance such
    as parental and sickness leave are financed by \emph{Kammarkollegiet}.
    There are no contributions paid to your pension account (either public or private)
    during this time.
    
    Occasionally, students continue to be funded by donation stipends in
    their second year (or a part thereof), but the income received
    is increased to approximately \sek{18,800} to be comparable to
    formal employment (see \ref{item:funding:doktorand}).
    
    Note that since you pay no taxes, you cannot benefit from various tax 
    deductions.
    
  \item \label{item:funding:doktorand}
    The main form of funding for most students is the so-called 
    \emph{doctoral studentship} or \emph{``Doktorandtjänst.''} 
    Students receiving 
    this funding are formally employed and thus entitled to the usual 
    social insurance benefits available to other Swedish employees. However,
    by law this form of funding is limited to the first 48 months of 
    full-time studies, \emph{regardless of whether a student actually received 
    Doktorandtjänst money during this time}. For example, a student who
    was financed by stipends in the first year can be financed by Doktorandtjänst
    only up to year 4, not 5.
    
    The income received from Doktorandtjänst depends on the fraction of doctoral
    studies you have completed:
  
    \begin{flushleft}
      \begin{tabularx}{.95\linewidth}{Xr}
        \toprule
        Percentage completed & Pre-tax income \\
        \midrule
        less than 50\% & \sek{24,100} \\
        50\%--80\% &  \sek{26,400} \\
        more than 80\% & \sek{28,000} \\
        \bottomrule
      \end{tabularx}
    \end{flushleft}
    
    Your supervisor decides which category is applicable.
    The exact net-of-tax amount will depend on where you live (which determines
    your tax rate) and how much other taxable income you have (\eg from TA and RA jobs).
    However, \sek{24,000} correspond to roughly \sek{19,000} after taxes.
    
    While the law states that Doktorandtjänst can be paid only for 4 years
    of full-time studies, teaching can be used to extend financing beyond
    the fourth year. If you choose to TA courses (usually from the second year
    onwards), you are regarded as a part-time student, 
    spending only 80\% of your time on studying and research. 
    The remaining 20\% are devoted to teaching. 
    Your salary will be adjusted accordingly (80\% of the amount given above and 20\%
    teaching salary). As a rule of thumb, funding is extended by six months for 
    every 70 hours you teach 
    (hours vary by course, since some courses are more time consuming).
    
    Note that you forfeit this funding extension if you leave the department within
    the first four years (\eg by transferring to \iies or \sofi).
    
  \item A third source of funding comes from grants/stipends obtained by 
    your supervisor or some other member of faculty. A major provider 
    of such funds is Handelsbanken, a big Swedish bank. Handelsbanken
    in addition finances contributions to private pension insurance, however,
    parental and sick leave are again paid for by \emph{Kammarkollegiet} (see
    \ref{item:funding:donation}).
    
    This form of funding is explicitly granted for research purposes, hence
    it will in general be available only to students in their later years who
    are done with course work.
    
    The exact amount received depends on the particular grant, which might
    impose limits on the amount of stipends doctoral students are allowed to
    receive. The income progression ladder mentioned in \ref{item:funding:doktorand}
    therefore is not applicable.
    
    Unlike with Doktorandtjänst, students can receive these stipends
    beyond the fourth year, but this depends on arrangements they make with
    their supervisor.
    
    Teaching is also treated differently: students
    on stipends get paid for teaching by the hour, but do not become eligible
    for funding extensions.
\end{enumerate}

\paragraph{Funding timeline.}

To summarize, the funding over your entire PhD (conditional on staying
at the department throughout) will look as follows:

\begin{description}
  \item[Years 1--2:] Your are initially funded by donation stipends and
    usually transition into formal employment (Doktorandtjänst) at some point
    in your second year.
    
  \item[Years 2--5:] You are funded either via Doktorandtjänst (until year 5
    if you earned extensions by teaching) or grants obtained by your supervisor.
    
  \item[Years 5--end:] You are funded by grants your supervisor may have obtained.
    There is no guarantee that such funds will be available to you.
\end{description}

\paragraph{Studying abroad.}
If you choose to go abroad (see \ref{sec:phd:abroad}), your funding period is not on hold,
even though you do not receive any funds from the \dept during that time. According to the
current formula, the department deducts half a year of funding for one year spent abroad,
\ie if you fund yourself from other sources for one academic year while abroad, 
the department will fund you for 3.5 years (assuming that you have no extension from 
teaching).

\paragraph{Additional funds.}
Each student has up to \sek{30,000} to spend on conferences and summer schools during
the first two years (pending supervisor approval). 
Additional funding may be provided by your supervisor in later years.

\subsection{\iies}

The funding scheme at the \iies is much simpler:
\begin{compactitem}
	\item If you start as an RA in your first year, you receive income
    comparable to first-year students at the department.
	\item Once you are admitted as a graduate student, you receive a non-taxable 
		stipend of \sek{18,000} per month (\sek{20,000} starting from July 2016), 
		which is drawn from your supervisor's funds. 
		Since you are not employed at \iies,
		and thus do not contribute to your pension benefits, you receive
		an additional \sek{50,000} a year which are to be invested in a
		savings account or fund at Handelsbanken (the exact rules are somewhat ambiguous).
  \item Just like department students funded by stipends, you are entitled
    to parental and sick leave financed by \emph{Kammarkollegiet}.
\end{compactitem}

\paragraph{Teaching.}
There is no obligation to teach, and teaching will not extend your funding.
Teaching salaries (which are taxable) are paid by the \dept on top of your \iies stipend.

\paragraph{Studying abroad.}
If you choose to go abroad on external funding, your \iies money will be put on hold.
The same rule applies if you do an internship, for example at the Riksbank (the Swedish central bank).

\paragraph{Additional funds.}
Additional funds for conferences, summer schools, \etc may be available. You have
to arrange for these with your supervisor.

\section{Studying abroad}
\label{sec:phd:abroad}

There are ample opportunities to study abroad during your PhD. 
Students usually go in their third, fourth or even fifth year, depending
on whether they want to attend courses or focus on research when abroad. The paragraphs 
below are intended to give you a broad idea of your options.

\subsection{US universities / Hedelius scholarship}
\label{sec:hedelius}

In the past, students have spent an academic year at top US universities such as Harvard, MIT, Yale, NYU, Columbia, Princeton and others. These are the things to keep in mind:
\begin{compactitem}
	\item
		\marginnote{The Hedelius scholarship website is located at \url{http://www.handelsbanken.se/shb/inet/IStartSv.nsf/FrameSet?OpenView&id=Forskningsstiftelserna}.}
		The main source of funding for most students is the Hedelius scholarship, which awards
		up to \sek{700,000} to study abroad. The application deadline is in September of the year
		\emph{prior} to the academic year when you actually want to go.
		
		Note that the Hedelius scholarship is not tied to going to US universities, 
		even though most 	students do. The LSE is one popular non-US alternative.
		
		Additionally, in the past students have visited US universities without
		receiving the Hedelius scholarship.
		
	\item Deadlines for applications to US universities vary widely, sometimes ending more than
		half a year before you actually want to go.
		
	\item If you need to secure an invitation from a faculty member of your host university,
		having a supervisor with contacts to faculty at a particular university is an advantage.
\end{compactitem}

\subsection{ENTER network}
\marginnote{Website: \url{http://www.enter-network.org}}
\marginnote[1em]{If you want to study abroad or present at partner universities, contact the current student ENTER coordinator at the \dept.}
Another alternative is to visit one of the participating universities within the ENTER program, 
which include
\begin{inparaenum}
	\item Universitat Autonoma de Barcelona;
	\item Université Libre de Bruxelles;
	\item UCL;
	\item Universidad Carlos III de Madrid;
	\item Universität Mannheim;
	\item Tilburg University; and
	\item Université de Toulouse 1.
\end{inparaenum}
Each institution also offers presentation slots in their seminar series to students from participating
universities. Additionally, universities take turns arranging an annual conference where students present their work.

\subsection{PODER network}
\marginnote{Website: \url{http://poder.cepr.org}}

Policy Design and Evaluation Research in Developing Countries (PODER) offers fellowships 
to PhD students who want to visit one of the participating universities besides \iies:
\begin{inparaenum}
	\item Bocconi University;
	\item Paris School of Economics
	\item Facultes Universitaires Notre-Dame de la Paix \`{a} Namur; and
	\item LSE.
\end{inparaenum}

Two types of fellowships are available for either Early Stage Researchers (ESRs) or 
for Experienced Researchers (ERs). ESRs are mainly meant for 2nd to 4th year PhD students, ERs for 5th year PhD students. 

The network gives students a chance to take courses and/or to conduct research during a 3--36 months period for ESRs or a 3--24 months period for ERs at one of the above universities.
