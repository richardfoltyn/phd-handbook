% Main document for the "Living in Sweden" part of the handbook.

% Define some useful shortcuts
\def\pn{personnummer\xspace}
\def\skv{Skatteverket\xspace}
\def\migv{Migrationsverket\xspace}


% Chapter on immigration
\input{immigration}

% Chapter on finding accommodation
\input{housing}


% Lump together miscellaneous stuff directly in here
\chapter{Other administrative issues}

% section on Swedish health-care system
\input{health}


\subsection{Other insurance}

\def\hemfors{hemf\"{o}rs\"{a}kring\xspace}

You can buy so-call ``\hemfors'' (home insurance) to insure
against theft, damaged property, \etc from various insurance providers.
If you have a first-hand contract with SSSB, you will be offered a \hemfors
package from Trygg Hansa at student prices that is tied to your SSSB housing.



\chapter{Miscellaneous}

\section{Swedish courses}

If you plan to study Swedish (which is useful, even though
you can survive with English), there are several options.

\paragraph{Courses at \su.}
  Swedish courses are offered at the Department for Swedish language at SU
    free of charge. 
        
  \marginnote{Website: \url{http://www.su.se/svefler/}}        

    They offer a sequence of about six courses, all of which except for the first one
    are held in groups of about 20 students. The introductory course is given
    in large lecture rooms for 200 students, so do not expect to learn much.
    After passing the initial exam, you basically start again from scratch
    in smaller groups. The remaining courses in the sequence should take you from
    A1 to a B2 language level.
    
    The quality of these courses is very mixed, as they are attended by
    numerous Erasmus students who come to Sweden for at most a year and thus
    have limited incentives to learn the language.

\paragraph{SFI.}
Free courses in Swedish are also offered to all immigrants as part of the program 
\emph{Swedish For Immigrants} (SFI). 

\marginnote{Website: \url{http://www.stockholm.se/sfi}.}

The evening courses usually involve 6 hours per week. Enrollment for residents in Stockholm takes place at the SFI Centre (Hornsgatan 124); you must have a \pn to register. Classes can be large, and experiences regarding the quality of these courses are mixed. 

Note that SFI is administered by the municipality you live in, so if you do not live
in Stockholm (\eg in areas such as T\"{a}by), you will have to contact the local
administration.

\paragraph{Private providers.}
\marginnote[1.5em]{Folkuniversitetet (\url{http://www.folkuniversitetet.se/}) and
  Medborgarskolan (\url{http://www.medborgarskolan.se/}) offer course information
  in English.}
Alternatively, there are a number of private schools in Stockholm such as Folkuniversitetet and Medborgarskolan that offer courses. 
Fees vary between \sek{500--4,000}, not including books and other material.
These course may also be offered over summer.


\section{Student union}
\label{sec:living:sus}
\marginnote{Website: \url{http://sus.su.se/}}
Every university has its own student union. As a student at \su 
you can become a member of \emph{Stockholms Universitets studentk\r{a}r} (SUS)
once have enrolled as a student.
\marginnote{The SUS office is located in ``student\-huset'' next to building A.}

The student membership is \sek{120} per term and offers some 
benefits:
\begin{compactitem}
  \item You can accumulate credit days in the SSSB housing queue (see
  \ref{sec:living:sssb}). When you live with SSSB, you \emph{must} be a student
  union member.
  \item There are some additional discounts available only to union members.
\end{compactitem}
If you do not have a \pn yet, you can become a member using a temporary
ID number assigned by the university. Be sure to update your \pn once
you have it, and to use the same email address as with SSSB.
\marginnote{You can order a plastic card at \url{http://www.studentkortet.se}.}
After paying your membership fee, you can order a plastic student card
(``studentkortet'') or use the mobile phone app.

Even if you do not want to become a student union member, you should get
the ``classic'' student card, since
\begin{compactitem}
  \item when traveling using discounted SL student tickets (see \ref{sec:living:sl}),
    you might be required to show your student card;
  \item several companies offer student discounts to card holders (including Arlanda
    Express).
    \marginnote[-2em]{See \url{http://www.studentkortet.se} for additional student discounts
    	and on how to order a plastic student card.}
\end{compactitem}
There seems to be an administrative fee of \sek{50} to obtain the classic card,
so there is no good reason not to get the union membership.

Note that after the first year 
the student union is unable to verify whether PhD students are 
eligible to become a members (\ie have enough ECTS credits). 
\marginnote{Student union membership is not renewed automatically!}
Therefore,
every term you need to get a confirmation from the program administrator
that you are a PhD student and email it to SUS (or drop by in their office).


\section{Bank account}
\label{sec:living:bank}

Getting a bank account without a \pn is hard but not impossible.
If you do not want to wait a few weeks until you receive your \pn, you might
be lucky enough to convince a bank to open an account for you. Some people
were successful in the SEB branch close to SSE, since that one deals with
many foreign students. 
You will get an account for non-residents (without a debit/credit card) 
and have to change that once you receive your \pn.

You should bring the following documents:
\begin{compactenum}
  \item passport or Swedish ID card;
  \item admission letter to the PhD program;
  \item some document from SU stating what stipend/salary you are entitled to.
\end{compactenum}

If you want to use your foreign credit card in Sweden, make
sure you know the PIN number. It is not needed in some other European
countries, but Swedish terminals might require it.

\section{ID card}

You should consider getting a Swedish ID card (``identitetskort'')
as it makes things easier:
\begin{compactitem}
  \item Using some services requires a Swedish ID card, a passport will
    not do (\eg getting a mobile phone contract). Some Swedish businesses
    insist on Swedish ID cards since these can be scanned automatically
    in shops, etc.
  \item You don't need to carry around a passport.
  \item The ID card shows your \pn, unlike your passport.
\end{compactitem}

\marginnote{Note that you can apply for an ID card only at some Skatteverket offices.
		In Stockholm this is currently the office at Lindhagensgatan 76 on Kungsholmen.}
You can get an ID card at the local \skv office for \sek{400}. 
Check what documents
you need online; in any case you do not need passport photos, since
digital pictures will be taken when you apply.

\section{e-legitimation}

e-legitimation is an electronic identification mechanism that can be
used to authenticate yourself when using various government services online.
For example, when moving you can change your living address 
in the Swedish register at \url{http://www.skatteverket.se} without
having to go there in person. Similarly, you can file your tax returns
online.

If you have a bank account, you most likely already have e-legitimation, 
either via the mobile BankID or your debit card and a smartcard reader
your received from your bank for online banking purposes. 
Alternatively, the ID card issued by Skatteverket also comes with e-legitimation, 
but you will need a smartcard reader.

\section{Mobile phone contract}

Getting a mobile phone contract is surprisingly difficult for foreigners.
Not only do you need a \pn, most telecom companies will also refuse 
to give you a contract without a Swedish ID card (or drivers license).
Additionally, you will need to have a credit record (or a taxable salary), 
which implies that you won't
be able to get a contract in the first few months after you received your \pn.

Initially you therefore have two options:
\begin{compactenum}
  \item Get a pre-paid card (``kontantkort'') which might
    include mobile data;
  \item Ask a colleague who has lived in Sweden for a while to sign 
    a contract for you. This contract can be transferred to your name later.
\end{compactenum} 

\section{Public transportation in Stockholm}
\label{sec:living:sl}

\marginnote[1em]{Website: \url{http://www.sl.se}}

SL, the public transportation company in Stockholm, offers 
discounted 30- and 90-day student travelcards for \sek{560} and \sek{1540}, respectively. 
These are valid throughout Stockholm county. These tickets must be loaded onto
an electronic card (SL Access card) which can be bought at SL counters and in Pressbyr\r{a}n stores.
\marginnote[-1em]{Once you have a \pn, you can register your card online to protect
against loss or theft.}
Once you have a card, you can recharge it at SL vending machines located at many
stops.

If you do not want to use a travelcard, you should instead charge your SL Access
card with travel credits (called ``reskassa'') in advance. 
The minimum charge is \sek{200},
but paying one-way tickets via reskassa costs only \sek{25} per trip, instead
of \sek{36} for regular one-way tickets.
